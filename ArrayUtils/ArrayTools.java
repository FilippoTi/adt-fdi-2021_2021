package ArrayUtils;

public class ArrayTools {

	public static Object[] resize(Object[] array, int newSize) {
		Object[] returned = new Object[newSize];

		if (array == null)
			return returned;

		for (int i = 0; i < newSize && i < array.length; i++) {
			returned[i] = array[i];
		}

		return returned;
	}

	public static void rShift(Object[] array) {
		if (array == null)
			throw new NullPointerException();

		for (int i = array.length - 1; i > 0; i--) {
			array[i] = array[i - 1];
		}
	}

	public static void lShift(Object[] array) {
		if (array == null)
			throw new NullPointerException();

		for (int i = 0; i < array.length - 1; i++) {
			array[i] = array[i + 1];
		}
	}

	public static void copy(Object[] src, int srcPos, Object[] dest, int destPos, int lenght) {
		if (src == null || dest == null) {
			throw new NullPointerException();
		}
		if (dest.length <= destPos + lenght) {
			throw new IndexOutOfBoundsException();
		}
		if (src.length <= srcPos + lenght) {
			throw new IndexOutOfBoundsException();
		}
		if (srcPos < 0 || srcPos >= src.length) {
			throw new IndexOutOfBoundsException();
		}
		if (destPos < 0 || destPos >= dest.length) {
			throw new IndexOutOfBoundsException();
		}

		for (int i = 0; i < lenght; i++) {
			dest[i + destPos] = src[i + srcPos];
		}
	}

	public static int binarySearch(Comparable[] array, int start, int end, Comparable target) {
		if (start > end)
			return -1;

		int mid = (start + end) / 2;
		if (array[mid].equals(target))
			return mid;

		if (target.compareTo(array[mid]) < 0) {
			return binarySearch(array, start, mid - 1, target);
		} else {
			return binarySearch(array, mid + 1, end, target);
		}

	}

	public static void inPlaceInsertionSort(Object[] array, boolean des) {
		if (array == null)
			throw new IllegalArgumentException();

		if (array.length == 0)
			return;

		if (!(array[0] instanceof Comparable))
			throw new IllegalArgumentException();

		for (int i = 0; i < array.length; i++) {
			Object selected = array[i];
			for (int j = 0; j < i; j++) {
				if (!des) {
					if (((Comparable) array[j]).compareTo((Comparable) selected) > 0) {
						for (int z = i - 1; z >= j; z--) {
							array[z + 1] = array[z];
						}

						array[j] = selected;
						break;
					}
				} else {
					if (((Comparable) array[j]).compareTo((Comparable) selected) < 0) {
						for (int z = i - 1; z >= j; z--) {
							array[z + 1] = array[z];
						}

						array[j] = selected;
						break;
					}
				}
			}
		}
	}

	public static Object[] InsertionSort(Object[] array, boolean des) {
		if (array == null)
			throw new IllegalArgumentException();

		if (array.length == 0)
			return new Object[0];

		if (!(array[0] instanceof Comparable))
			throw new IllegalArgumentException();

		Object[] clone = new Object[array.length];
		System.arraycopy(array, 0, clone, 0, array.length);
		inPlaceInsertionSort(clone, des);

		return clone;
	}

	public static Object[] Merge(Object[] Array1, Object[] Array2, boolean des) {

		int left = 0, right = 0, counter = 0;

		Object[] merged = new Object[Array1.length + Array2.length];

		while (left < Array1.length && right < Array2.length) {
			if (!des) {
				if (((Comparable) Array1[left]).compareTo(Array2[right]) <= 0) {
					merged[counter] = Array1[left];
					left++;
				} else {
					merged[counter] = Array2[right];
					right++;
				}
			} else {
				if (((Comparable) Array1[left]).compareTo(Array2[right]) >= 0) {
					merged[counter] = Array1[left];
					left++;
				} else {
					merged[counter] = Array2[right];
					right++;
				}
			}

			counter++;
		}

		while (left < Array1.length) {
			merged[counter] = Array1[left];
			left++;
			counter++;
		}
		while (right < Array2.length) {
			merged[counter] = Array2[right];
			right++;
			counter++;
		}

		return merged;
	}

	public static Object[] MergeSort(Object[] array, boolean des) {
		if (array.length > 1) {
			Object[] left = new Object[array.length / 2];
			Object[] right = new Object[array.length - left.length];
			System.arraycopy(array, 0, left, 0, array.length / 2);
			System.arraycopy(array, array.length / 2, right, 0, array.length - left.length);
			return Merge(MergeSort(left, des), MergeSort(right, des), des);
		}

		return array;
	}


}
