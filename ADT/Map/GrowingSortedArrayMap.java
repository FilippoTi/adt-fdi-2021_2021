package ADT.Map;

public class GrowingSortedArrayMap extends FixedSortedArrayMap {


    @Override
    public Object put(Object key, Object value) {

        if (this.arraySize == this.array.length) {
            Pair[] tmp = new Pair[this.arraySize * 2];
            System.arraycopy(array, 0, tmp, 0, this.arraySize);
            array = tmp;
        }
        return super.put(key, value);
    }
}
