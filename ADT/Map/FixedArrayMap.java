package ADT.Map;

import ADT.Map.Exceptions.FullMapException;
import ArrayUtils.ArrayTools;

public class FixedArrayMap implements Map {
    protected Pair[] array;
    protected int arraySize;

    protected final int INIT_SIZE = 3;

    public FixedArrayMap() {
        array = new Pair[INIT_SIZE];
        makeEmpty();
    }

    @Override
    public void makeEmpty() {
        arraySize = 0;
    }

    @Override
    public boolean isEmpty() {
        return arraySize == 0;
    }


    @Override
    public Object put(Object key, Object value) {
        if (key == null || value == null)
            throw new IllegalArgumentException();

        Object oldValue = remove(key);

        if (arraySize == this.array.length) {
            throw new FullMapException();
        }
        array[arraySize++] = new Pair(key, value);

        return oldValue;
    }

    @Override
    public Object get(Object key) {
        if (key == null)
            throw new IllegalArgumentException();

        for (int i = 0; i < arraySize; i++) {
            if (array[i].getKey().equals(key)) {
                return array[i].getValue();
            }
        }

        return null;
    }

    @Override
    public Object remove(Object key) {
        for (int i = 0; i < arraySize; i++) {
            if (array[i].getKey().equals(key)) {
                Object old = array[i].getValue();
                array[i] = array[arraySize - 1];
                arraySize--;
                return old;
            }
        }

        return null;
    }

    @Override
    public Object[] keys() {
        Object[] keys = new Object[arraySize];
        for (int i = 0; i < arraySize; i++) {
            keys[i] = array[i].getKey();
        }

        return keys;
    }

    public int size() {
        return this.arraySize;
    }

    protected class Pair {
        private Object key;
        private Object value;

        public Pair() {
            key = null;
            value = null;
        }

        public Pair(Object key, Object value) {
            setKey(key);
            setValue(value);
        }

        public void setKey(Object key) {
            this.key = key;
        }

        public Object getKey() {
            return key;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public Object getValue() {
            return value;
        }
    }
}
