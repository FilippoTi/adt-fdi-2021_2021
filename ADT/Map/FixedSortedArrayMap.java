package ADT.Map;

import ADT.Map.Exceptions.FullMapException;
import ArrayUtils.ArrayTools;

public class FixedSortedArrayMap implements SortedMap {

    protected Pair[] array;
    protected int arraySize;

    protected final int INIT_SIZE = 3;

    public FixedSortedArrayMap() {
        this.array = new Pair[this.INIT_SIZE];
        makeEmpty();
    }

    public int size() {
        return this.arraySize;
    }

    @Override
    public void makeEmpty() {
        arraySize = 0;
    }

    @Override
    public boolean isEmpty() {
        return arraySize == 0;
    }

    @Override
    public Object put(Object key, Object value) {

        if (value == null || !(key instanceof Comparable)) throw new IllegalArgumentException();

        int pos = this.BinarySearch((Comparable) key);

        Comparable comparableKey = (Comparable) key;

        if (pos >= 0) {
            Object old = array[pos].getValue();
            array[pos] = new Pair(comparableKey, value);
            return old;
        }

        if (this.arraySize == this.array.length) throw new FullMapException();
        int i;
        for (i = this.arraySize - 1; i >= 0 && comparableKey.compareTo(array[i].getKey()) < 0; i--) {
            array[i + 1] = array[i];
        }

        array[i + 1] = new Pair(comparableKey, value);
        this.arraySize++;

        return null;
    }

    @Override
    public Object get(Object key) {
        int pos = this.BinarySearch((Comparable) key);

        if (pos < 0) return null;

        return array[pos].getValue();
    }

    @Override
    public Object remove(Object key) {
        int pos = this.BinarySearch((Comparable) key);

        if (pos < 0) return null;

        Object old = array[pos].getValue();

        for (int i = pos; i < this.arraySize - 1; i++) {
            array[pos] = array[pos + 1];
        }
        this.arraySize--;
        return old;
    }

    @Override
    public Object[] keys() {
        return sortedKeys();
    }

    @Override
    public Comparable[] sortedKeys() {
        Comparable[] tmp = new Comparable[this.arraySize];

        for (int i = 0; i < this.arraySize; i++) {
            tmp[i] = (Comparable) this.array[i].getKey();
        }

        return tmp;
    }

    private int BinarySearch(Comparable key) {

        if (!(key instanceof Comparable)) return -1;

        return ArrayTools.binarySearch(sortedKeys(), 0, this.arraySize - 1, key);

    }

    protected class Pair {
        private Comparable key;
        private Object value;

        public Pair() {
            key = null;
            value = null;
        }

        public Pair(Comparable key, Object value) {
            setKey(key);
            setValue(value);
        }

        public void setKey(Comparable key) {
            this.key = key;
        }

        public Comparable getKey() {
            return key;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public Object getValue() {
            return value;
        }
    }
}
