package ADT.Map;

import ADT.Map.Map;

public interface SortedMap extends Map {
    Comparable[] sortedKeys();
}
