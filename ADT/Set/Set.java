package ADT.Set;

import ADT.Container;

public interface Set extends Container {

	void add(Object obj);

	boolean contains(Object obj);

	Object[] toArray();
}
