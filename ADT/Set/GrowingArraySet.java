package ADT.Set;

public class GrowingArraySet implements Set {

	protected Object[] array;
	protected int arraySize;

	protected final int INIT_SIZE = 10;

	@Override
	public void makeEmpty() {
		arraySize = 0;
	}

	@Override
	public boolean isEmpty() {
		return arraySize == 0;
	}

	@Override
	public void add(Object obj) {
		if (obj == null)
			throw new IllegalArgumentException();

		if (contains(obj))
			return;

		if (arraySize == array.length) {
			Object[] tmp = new Object[array.length * 2];
			System.arraycopy(array, 0, tmp, 0, arraySize);
			array = tmp;
		}

		array[arraySize] = obj;
		arraySize++;
	}

	@Override
	public boolean contains(Object obj) {
		if (obj == null)
			throw new IllegalArgumentException();

		for (int i = 0; i < arraySize; i++) {
			if (array[i].equals(obj))
				return true;
		}

		return false;
	}

	@Override
	public Object[] toArray() {
		Object[] tmp = new Object[arraySize];

		for (int i = 0; i < arraySize; i++) {
			tmp[i] = array[i];
		}

		return tmp;
	}

	public static GrowingArraySet union(GrowingArraySet left, GrowingArraySet right) { // O(n^2)

		if (left == null && right == null)
			throw new IllegalArgumentException();

		if (left == null)
			return right;

		if (right == null)
			return left;

		GrowingArraySet tmp = new GrowingArraySet();
		Object[] leftA = left.toArray();
		Object[] rightA = right.toArray();

		for (int i = 0; i < leftA.length; i++) {
			tmp.add(leftA[i]);
		}
		for (int i = 0; i < rightA.length; i++) {
			tmp.add(rightA[i]);
		}

		return tmp;
	}

	public static GrowingArraySet intersection(GrowingArraySet left, GrowingArraySet right) { // O(n^2)

		if (left == null || right == null)
			return null;

		GrowingArraySet tmp = new GrowingArraySet();
		Object[] leftA = left.toArray();

		for (int i = 0; i < leftA.length; i++) {
			if (right.contains(leftA[i])) {
				tmp.add(leftA[i]);
			}
		}

		return tmp;
	}

	public static GrowingArraySet diff(GrowingArraySet left, GrowingArraySet right) { // O(n^2)

		if (left == null && right == null)
			throw new IllegalArgumentException();

		if (left == null)
			return null;

		if (right == null)
			return left;

		GrowingArraySet tmp = new GrowingArraySet();
		Object[] leftA = left.toArray();

		for (int i = 0; i < leftA.length; i++) {
			if (!right.contains(leftA[i])) {
				tmp.add(leftA[i]);
			}
		}

		return tmp;
	}

}
