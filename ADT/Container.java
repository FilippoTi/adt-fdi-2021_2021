package ADT;

public interface Container {
    void makeEmpty();

    boolean isEmpty();
}
