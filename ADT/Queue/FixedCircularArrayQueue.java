package ADT.Queue;

import ADT.Queue.Exceptions.FullQueueException;

public class FixedCircularArrayQueue extends FixedArrayQueue {
    protected int increment(int index) {
        return (index + 1) % this.array.length;
    }

    @Override
    public void enqueue(Object obj) {
        if (increment(back) == front) {
            throw new FullQueueException();
        }
        array[back] = obj;
        back = increment(back);
    }

    @Override
    public Object dequeue() {
        Object tmp = getFront();
        front = increment(front);
        return tmp;
    }

    @Override
    public String toString() {
        return "Front=" + front + "; " + "Back=" + back + "; " + super.toString();
    }

    @Override
    public String Debug() {
        String content = "";
        for (int i = this.front; i != back; i = increment(i)) {
            content += array[i].toString() + "\n";
        }

        return this + "\n" + content;
    }
}
