package ADT.Queue;

import ArrayUtils.ArrayTools;

public class GrowingCircularArrayQueue extends FixedCircularArrayQueue {
    @Override
    public void enqueue(Object obj) {
        if (increment(back) == front) {
            this.array = ArrayTools.resize(array, this.array.length * 2);
            if (back < front) {
                System.arraycopy(array, 0, array, array.length / 2, back);
                back += array.length / 2;
            }
        }
        super.enqueue(obj);
    }
}
