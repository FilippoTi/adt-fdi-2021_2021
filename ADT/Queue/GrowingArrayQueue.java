package ADT.Queue;

import ADT.Queue.Exceptions.FullQueueException;
import ArrayUtils.ArrayTools;

public class GrowingArrayQueue extends FixedArrayQueue {
    @Override
    public void enqueue(Object obj) {
        if (back == this.array.length) {
            this.array = ArrayTools.resize(array, this.array.length * 2);
        }
        super.enqueue(obj);
    }
}
