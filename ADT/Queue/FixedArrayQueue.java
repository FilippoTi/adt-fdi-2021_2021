package ADT.Queue;

import ADT.Queue.Exceptions.EmptyQueueException;
import ADT.Queue.Exceptions.FullQueueException;

public class FixedArrayQueue implements Queue {

    protected Object[] array;
    protected int front;
    protected int back;

    protected final int INIT_SIZE = 3;

    public FixedArrayQueue() {
        array = new Object[INIT_SIZE];
        makeEmpty();
    }

    @Override
    public void makeEmpty() {
        front = 0;
        back = 0;
    }

    @Override
    public boolean isEmpty() {
        return front == back;
    }

    @Override
    public void enqueue(Object obj) {
        if (back == this.array.length) {
            throw new FullQueueException();
        }
        array[back] = obj;
        back++;
    }

    @Override
    public Object dequeue() {
        Object tmp = getFront();
        front++;
        return tmp;
    }

    @Override
    public Object getFront() {
        if (this.isEmpty()) {
            throw new EmptyQueueException();
        }
        return array[front];
    }

    @Override
    public String toString() {
        return "RS=" + this.array.length + "; VS=" + (this.back - this.front) + "; " + super.toString();
    }

    public String Debug() {
        String content = "";
        for (int i = this.front; i < back; i++) {
            content += array[i].toString() + "\n";
        }

        return this + "\n" + content;
    }
}
