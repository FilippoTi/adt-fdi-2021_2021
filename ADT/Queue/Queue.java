package ADT.Queue;

import ADT.Container;

public interface Queue extends Container {
    void enqueue(Object obj);

    Object dequeue();

    Object getFront();
}
