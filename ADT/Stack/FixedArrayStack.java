package ADT.Stack;

import ADT.Stack.Exceptions.EmptyStackException;
import ADT.Stack.Exceptions.FullStackException;

public class FixedArrayStack implements Stack {

    protected Object[] array;
    protected int arraySize;

    protected final int INIT_SIZE = 100;

    public FixedArrayStack() {
        array = new Object[INIT_SIZE];
        this.makeEmpty();
    }

    @Override
    public void makeEmpty() {
        this.arraySize = 0;
    }

    @Override
    public boolean isEmpty() {
        return (arraySize == 0);
    }

    @Override
    public void push(Object obj) {
        if (this.arraySize == this.array.length) {
            throw new FullStackException();
        }
        this.array[arraySize] = obj;
        arraySize++;
    }

    @Override
    public Object top() {
        if (this.isEmpty()) {
            throw new EmptyStackException();
        }
        return this.array[this.arraySize - 1];
    }

    @Override
    public Object pop() {
        Object tmp = this.top();
        arraySize--;
        return tmp;
    }

    @Override
    public String toString() {
        return "RS=" + this.array.length + "; VS=" + this.arraySize + "; " + super.toString();
    }

    public String Debug() {
        String content = "";
        for (int i = this.arraySize - 1; i >= 0; i--) {
            content += array[i].toString() + "\n";
        }

        return this + "\n" + content;
    }
}
