package ADT.Stack;

import ArrayUtils.ArrayTools;

public class GrowingArrayStack extends FixedArrayStack {
    @Override
    public void push(Object obj) {
        if (this.arraySize == this.array.length) {
            this.array = ArrayTools.resize(this.array, 2 * arraySize);
        }
        super.push(obj);
    }
}
