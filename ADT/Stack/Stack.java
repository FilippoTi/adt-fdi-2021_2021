package ADT.Stack;

import ADT.Container;

public interface Stack extends Container {
    void push(Object obj);

    Object pop();

    Object top();
}
