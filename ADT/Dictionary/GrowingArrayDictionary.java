package ADT.Dictionary;

import ADT.Dictionary.Exceptions.FullDictionaryException;

public class GrowingArrayDictionary extends FixedArrayDictionary {

	@Override
	public void insert(Object key, Object value) {
		if (key == null || value == null)
			throw new IllegalArgumentException();

		if (this.arraySize == array.length) {
			Pair[] tmp = new Pair[arraySize * 2];
			System.arraycopy(array, 0, tmp, 0, arraySize);
			array = tmp;
		}

		super.insert(key, value);
	}

}
