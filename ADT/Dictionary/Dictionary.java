package ADT.Dictionary;

import ADT.Container;

public interface Dictionary extends Container {

	public void insert(Object key, Object value);

	public Object remove(Object key);

	public Object find(Object key);

	public Object[] findAll(Object key);

	public Object[] removeAll(Object key);

	public Object[] keys();

}
