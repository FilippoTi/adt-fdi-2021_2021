package ADT.Dictionary;

import ADT.Dictionary.Exceptions.FullDictionaryException;

public class FixedArrayDictionary implements Dictionary {

	protected Pair[] array;
	protected int arraySize;

	protected final int INIT_SIZE = 100;

	public FixedArrayDictionary() {
		array = new Pair[INIT_SIZE];
		arraySize = 0;

		makeEmpty();
	}

	@Override
	public void makeEmpty() {
		arraySize = 0;
	}

	@Override
	public boolean isEmpty() {
		return arraySize == 0;
	}

	public int size() {
		return this.arraySize;
	}

	@Override
	public void insert(Object key, Object value) {
		if (key == null || value == null)
			throw new IllegalArgumentException();

		if (this.arraySize == array.length)
			throw new FullDictionaryException();

		array[arraySize] = new Pair(key, value);
		arraySize++;
	}

	@Override
	public Object remove(Object key) {
		if (key == null)
			throw new IllegalArgumentException();

		for (int i = 0; i < this.arraySize; i++) {
			if (array[i].getKey().equals(key)) {
				Object old = array[i].getValue();
				array[i] = array[arraySize - 1];
				arraySize--;
				return old;
			}
		}
		return null;
	}

	@Override
	public Object find(Object key) {
		if (key == null)
			throw new IllegalArgumentException();

		for (int i = 0; i < this.arraySize; i++) {
			if (array[i].getKey().equals(key)) {
				return array[i].getValue();
			}
		}
		return null;
	}

	@Override
	public Object[] findAll(Object key) {
		if (key == null)
			throw new IllegalArgumentException();

		Object[] values = new Object[3];
		int valuesSize = 0;

		for (int i = 0; i < this.arraySize; i++) {
			if (array[i].getKey().equals(key)) {
				if (valuesSize == values.length) {
					Object[] tmp = new Object[valuesSize * 2];
					System.arraycopy(values, 0, tmp, 0, valuesSize);
					values = tmp;
				}

				values[valuesSize] = array[i].getValue();
				valuesSize++;
			}
		}

		if (valuesSize == 0)
			return null;

		Object[] tmp = new Object[valuesSize];
		System.arraycopy(values, 0, tmp, 0, valuesSize);
		values = tmp;

		return values;
	}

	@Override
	public Object[] removeAll(Object key) {
		if (key == null)
			throw new IllegalArgumentException();

		Object[] values = new Object[3];
		int valuesSize = 0;

		for (int i = 0; i < this.arraySize; i++) {
			if (array[i].getKey().equals(key)) {
				if (valuesSize == values.length) {
					Object[] tmp = new Object[valuesSize * 2];
					System.arraycopy(values, 0, tmp, 0, valuesSize);
					values = tmp;
				}

				values[valuesSize] = array[i].getValue();
				array[i] = array[this.arraySize - 1];
				this.arraySize--;
				valuesSize++;
				i--;
			}
		}

		if (valuesSize == 0)
			return null;

		Object[] tmp = new Object[valuesSize];
		System.arraycopy(values, 0, tmp, 0, valuesSize);
		values = tmp;

		return values;
	}

	@Override
	public Object[] keys() {
		Object[] keys = new Object[this.arraySize];

		for (int i = 0; i < arraySize; i++) {
			keys[i] = array[i].getKey();
		}
		
		return keys;
	}

	protected class Pair {
		protected Object key;
		protected Object value;

		public Pair() {
			setKey(null);
			setValue(null);
		}

		public Pair(Object key, Object value) {
			setKey(key);
			setValue(value);
		}

		public Object getKey() {
			return key;
		}

		public void setKey(Object key) {
			this.key = key;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "Pair [key=" + key + ", value=" + value + "]: " + super.toString();
		}

	}

}
