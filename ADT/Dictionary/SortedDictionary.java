package ADT.Dictionary;

public interface SortedDictionary extends Dictionary {

	public Object[] sortedKeys();
}
