package ADT.Dictionary;

import ADT.Dictionary.Exceptions.FullDictionaryException;
import ArrayUtils.ArrayTools;

public class FixedSortedArrayDictionary implements SortedDictionary {

	protected Pair[] array;
	protected int arraySize;

	protected final int INIT_SIZE = 5;

	public FixedSortedArrayDictionary() {
		array = new Pair[INIT_SIZE];
		arraySize = 0;

		makeEmpty();
	}

	@Override
	public void makeEmpty() {
		arraySize = 0;
	}

	@Override
	public boolean isEmpty() {
		return arraySize == 0;
	}

	public int size() {
		return this.arraySize;
	}

	@Override
	public void insert(Object key, Object value) {
		if (key == null || value == null)
			throw new IllegalArgumentException();

		if (!(key instanceof Comparable))
			throw new IllegalArgumentException();

		if (this.arraySize == array.length)
			throw new FullDictionaryException();

		int i = arraySize - 1;
		while (i >= 0 && ((Comparable) array[i].getKey()).compareTo(key) > 0) {
			array[i + 1] = array[i];
			i--;
		}

		array[i + 1] = new Pair(key, value);
		arraySize++;

	}

	private int binarySearch(Object target) {
		if (!(target instanceof Comparable))
			throw new IllegalArgumentException();

		return ArrayTools.binarySearch(sortedKeys(), 0, arraySize - 1, (Comparable) target);
	}

	private int binarySearchFirstElement(Object target) {
		if (!(target instanceof Comparable))
			throw new IllegalArgumentException();

		int pos = ArrayTools.binarySearch(sortedKeys(), 0, arraySize - 1, (Comparable) target);

		int i = pos;
		while (i > 0 && array[i - 1].getKey().equals(array[pos].getKey())) {
			i--;
		}

		return i;
	}

	@Override
	public Object remove(Object key) {
		if (key == null)
			throw new IllegalArgumentException();

		if (!(key instanceof Comparable))
			throw new IllegalArgumentException();

		int pos = binarySearch(key);

		if (pos < 0)
			return null;

		Object old = array[pos].getValue();

		for (int j = pos; j < arraySize - 1; j++) {
			array[j] = array[j + 1];
		}
		arraySize--;
		return old;

	}

	@Override
	public Object find(Object key) {
		if (key == null)
			throw new IllegalArgumentException();

		if (!(key instanceof Comparable))
			throw new IllegalArgumentException();

		int pos = binarySearch(key);

		if (pos < 0)
			return null;

		return array[pos].getValue();
	}

	@Override
	public Object[] findAll(Object key) {
		if (key == null)
			throw new IllegalArgumentException();

		if (!(key instanceof Comparable))
			throw new IllegalArgumentException();

		Object[] values = new Object[3];
		int valuesSize = 0;

		int first = binarySearchFirstElement(key);

		if (first < 0)
			return null;

		int i = first;
		do {
			if (valuesSize == values.length) {
				Object[] tmp = new Object[valuesSize * 2];
				System.arraycopy(values, 0, tmp, 0, valuesSize);
				values = tmp;
			}
			values[valuesSize] = array[i].getValue();
			valuesSize++;

			i++;

		} while (array[i].getKey().equals(array[first].getKey()));

		if (valuesSize == 0)
			return null;

		Object[] tmp = new Object[valuesSize];
		System.arraycopy(values, 0, tmp, 0, valuesSize);
		values = tmp;

		return values;
	}

	@Override
	public Object[] removeAll(Object key) {
		if (key == null)
			throw new IllegalArgumentException();

		if (!(key instanceof Comparable))
			throw new IllegalArgumentException();

		Object[] values = new Object[3];
		int valuesSize = 0;

		int pos = binarySearchFirstElement(key);
		Object chiave = array[pos].getKey();

		do {
			if (valuesSize == values.length) {
				Object[] tmp = new Object[valuesSize * 2];
				System.arraycopy(values, 0, tmp, 0, valuesSize);
				values = tmp;
			}
			values[valuesSize] = array[pos].getValue();
			valuesSize++;

			for (int j = pos; j < arraySize - 1; j++) {
				array[j] = array[j + 1];
			}
			arraySize--;

		} while (array[pos].getKey().equals(chiave));

		if (valuesSize == 0)
			return null;

		Object[] tmp = new Object[valuesSize];
		System.arraycopy(values, 0, tmp, 0, valuesSize);
		values = tmp;

		return values;
	}

	@Override
	public Object[] keys() {
		return sortedKeys();
	}

	@Override
	public Comparable[] sortedKeys() {
		Comparable[] keys = new Comparable[this.arraySize];

		for (int i = 0; i < arraySize; i++) {
			keys[i] = (Comparable) array[i].getKey();
		}

		return keys;
	}

	protected class Pair {
		protected Object key;
		protected Object value;

		public Pair() {
			setKey(null);
			setValue(null);
		}

		public Pair(Object key, Object value) {
			setKey(key);
			setValue(value);
		}

		public Object getKey() {
			return key;
		}

		public void setKey(Object key) {
			this.key = key;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "Pair [key=" + key + ", value=" + value + "]: " + super.toString();
		}

	}

}
