package ADT;

import ADT.Dictionary.FixedArrayDictionary;
import ADT.Dictionary.FixedSortedArrayDictionary;
import ADT.Dictionary.GrowingArrayDictionary;
import ADT.Dictionary.GrowingSortedArrayDictionary;
import ADT.Map.FixedArrayMap;
import ADT.Map.FixedSortedArrayMap;
import ADT.Map.GrowingArrayMap;
import ADT.Map.GrowingSortedArrayMap;
import ADT.Queue.FixedArrayQueue;
import ADT.Queue.FixedCircularArrayQueue;
import ADT.Queue.GrowingArrayQueue;
import ADT.Queue.GrowingCircularArrayQueue;
import ADT.Stack.GrowingArrayStack;
import ArrayUtils.ArrayTools;

import java.awt.Rectangle;
import java.util.Random;
import java.util.Scanner;

import org.w3c.dom.css.Rect;

public class Main {

    public static void main(String[] args) {

        //BIN Search Test
        Integer[] arrayBrutto = {1, 3, 9, 11, 26, 62};

        int pos = ArrayTools.binarySearch(arrayBrutto, 0, arrayBrutto.length - 1, -1);
        System.out.println(pos+"\n");
        
        Integer[] arrayBello = {5, 1, 3, 12, -3, 33, 1, 0};
        //Rectangle[] arrayBello = {new Rectangle(1, 2), new Rectangle(2, 3)};
        for(int ls = 0; ls < arrayBello.length; ls++)
        	System.out.print(arrayBello[ls]+" ");
        
        System.out.print("\n");
        
        ArrayTools.lShift(arrayBello);

        //ArrayTools.inPlaceInsertionSort(arrayBello, false);
        for(int ls = 0; ls < arrayBello.length; ls++)
        	System.out.print(arrayBello[ls]+" ");
        
        System.out.print("\n");
        //Object[] arrayGiusto = ArrayTools.MergeSort(arrayBello, true);
        /*for(int ls = 0; ls < arrayBello.length; ls++)
        	System.out.println(arrayBello[ls]);
        
        System.out.print("\n");
        
        for(int ls = 0; ls < arrayGiusto.length; ls++)
        	System.out.println(arrayGiusto[ls]);*/

        /*TEST STACK*/
        System.out.println("---------------------------------------------------");
        System.out.println("------------Start Array Stack Test-----------------");

        /*
        GrowingArrayStack pila = new GrowingArrayStack();
        System.out.println(pila.isEmpty());
        pila.push(1);
        System.out.println(pila.isEmpty());
        System.out.println(pila);
        pila.push(2);
        System.out.println(pila);
        pila.push(3);
        System.out.println(pila);
        pila.push(4);
        pila.push(5);
        pila.pop();
        pila.pop();
        System.out.println(pila.Debug());*/
        System.out.println("---------------------------------------------------");
        System.out.println("------------Ending Array Stack Test----------------\n");

        /*TEST QUEUE*/
        System.out.println("---------------------------------------------------");
        System.out.println("------------Start Array Queue Test-----------------");

        /*GrowingCircularArrayQueue coda = new GrowingCircularArrayQueue();
        System.out.println(coda.Debug());
        coda.enqueue(1);
        coda.enqueue(2);
        coda.enqueue(3);
        coda.enqueue(2);
        coda.enqueue(3);
        coda.enqueue(2);
        coda.enqueue(3);
        coda.enqueue(2);
        coda.enqueue(3);
        System.out.println(coda.Debug());
        coda.dequeue();
        System.out.println(coda.Debug());
        coda.enqueue(4);
        System.out.println(coda.Debug());*/

        System.out.println("---------------------------------------------------");
        System.out.println("------------Ending Array Queue Test----------------\n");


        System.out.println("---------------------------------------------------");
        System.out.println("-------------Start Array Map Test------------------");
        /*FixedArrayMap m = new GrowingArrayMap();

        String s = "Una stringa di prova per testare la mappa con una stringa";
        Scanner scan = new Scanner(s);

        while (scan.hasNext()) {
            String str = scan.next();
            System.out.println("Inserisco (" + str + "," + str.length() + ")");
            Object dup = m.put(str, str.length());
            if (dup != null) {
                System.out.println("Duplicato!");
            }
        }

        System.out.println("Numero di elementi introdotti " + m.size());

        System.out.println("Cerco la lunghezza associata alla parola \"prova\"");
        Object obj = m.get("prova");
        if (obj == null) {
            System.out.println("La parola cercata non esiste nella mappa");
        } else {
            System.out.println("la lunghezza e' " + (Integer) obj);
        }

        System.out.println("Rimuovo l'associazione con chiave \"prova\"");
        m.remove("prova");

        System.out.println("Cerco la lunghezza associata alla parola \"prova\"");
        obj = m.get("prova");
        if (obj == null) {
            System.out.println("La parola cercata non esiste nella mappa");
        } else {
            System.out.println("la lunghezza e' " + (Integer) obj);
        }

        System.out.println("Numero di elementi introdotti " + m.size());
        m.makeEmpty();
        System.out.println("Numero di elementi introdotti " + m.size());

        scan.close();*/
        System.out.println("-------------Ending Array Map Test-----------------");
        System.out.println("---------------------------------------------------\n");

        System.out.println("-----------Start SortedArray Map Test--------------");
        System.out.println("---------------------------------------------------");

        /*
        FixedSortedArrayMap map = new GrowingSortedArrayMap();

        String st = "Una stringa di prova per testare la mappa con una stringa con duplicati";
        Scanner scanner = new Scanner(st);

        while (scanner.hasNext()) {
            String str = scanner.next();
            System.out.println("Inserisco (" + str + "," + str.length() + ")");
            Object dup = map.put(str, str.length());
            if (dup != null) {
                System.out.println("Duplicato!");
            }
        }

        System.out.println("Numero di elementi introdotti " + map.size());

        System.out.println("Cerco la lunghezza associata alla parola \"prova\"");
        Object object = map.get("prova");
        if (object == null) {
            System.out.println("La parola cercata non esiste nella mappa");
        } else {
            System.out.println("la lunghezza e' " + (Integer) object);
        }

        System.out.println("Rimuovo l'associazione con chiave \"prova\"");
        map.remove("prova");

        System.out.println("Cerco la lunghezza associata alla parola \"prova\"");
        object = map.get("prova");
        if (object == null) {
            System.out.println("La parola cercata non esiste nella mappa");
        } else {
            System.out.println("la lunghezza e' " + (Integer) object);
        }

        Comparable[] k = map.sortedKeys();
        System.out.println("Stampo le chiavi (che saranno in ordine)");
        for (int i = 0; i < k.length; i++) {
            System.out.println(k[i]);
        }

        scanner.close();*/
        System.out.println("-----------Ending SortedArray Map Test---------------");
        System.out.println("---------------------------------------------------\n");
        
       /* FixedSortedArrayDictionary m = new GrowingSortedArrayDictionary();
        Random rand = new Random(123); //fisso il seed per debugging
        
        String s = "Una stringa di prova per testare la mappa con una stringa con duplicati";
        Scanner scan = new Scanner(s);
        
        while(scan.hasNext()){
           String str = scan.next();
           int value = rand.nextInt(100);
           System.out.println("Inserisco ("+str+","+value+")");
           m.insert(str,value);
        }
        
        System.out.println("Numero di elementi introdotti "+m.size());
        
        System.out.println("Stampo le chiavi");
        Object[] da = m.keys();
        for(int i=0;i<da.length;i++){
              System.out.println((String)da[i]);
        }
       
        System.out.println("Cerco il valore associato alla parola \"prova\"");
        Object obj = m.find("prova");
        if(obj == null){
           System.out.println("La parola cercata non esiste nella multimappa");
        }
        else{
           System.out.println("in valore associato e'"+(Integer)obj);
        }
        
        System.out.println("Rimuovo una associazione con chiave \"prova\"");
        m.remove("prova");
        
        System.out.println("Cerco il valore associato alla parola \"prova\"");
        obj = m.find("prova");
        if(obj == null){
           System.out.println("La parola cercata non esiste nella multimappa");
        }
        else{
           System.out.println("la lunghezza e' "+(Integer)obj);
        }
        
        System.out.println("Cerco i valori associato alla parola \"con\"");
        Object[] values = m.findAll("con");
        if(values.length == 0){
           System.out.println("La parola cercata non esiste nella multimappa");
        }
        else{
           for(int i=0;i<values.length;i++){
              System.out.println((Integer)values[i]);
           }   
        }
        System.out.println("Rimuovo una associazione con chiave \"con\"");
        m.removeAll("con");
        System.out.println("Cerco i valori associato alla parola \"con\"");
        values = m.findAll("con");
        if(values == null || values.length == 0){
           System.out.println("La parola cercata non esiste nella multimappa");
        }
        else{
           for(int i=0;i<values.length;i++){
              System.out.println((Integer)values[i]);
           }   
        }
        
        System.out.println("Stampo le chiavi");
        Object[] k = m.keys();
        for(int i=0;i<k.length;i++){
              System.out.println((String)k[i]);
        }
        
        scan.close();*/
    }
}
